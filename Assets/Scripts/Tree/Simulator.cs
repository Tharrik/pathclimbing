﻿using System.Collections.Generic;


public class Simulator
{
    public int depth;

    Grid grid;

    // List that contains all visited states
    List<Grid> visitedStates;

    StateNode lastNode;
    Path evolutionPath;
    
    public bool Completed{ get; private set; }

    public int GenerationCount { get; private set; }


    public Simulator(Grid inGrid, int inDepth)
    {
        grid = inGrid;
        depth = inDepth;
        visitedStates = new List<Grid>();
        lastNode = new StateNode(grid);
        evolutionPath = new Path();
    }

    /**
     * @param resultPath represents the best path of this generation
     * @return true if the generation could execute (the simulation was not complete)
     */
    public bool NextGeneration(ref Path resultPath)
    {
        if (Completed)
            return false;

        ++GenerationCount;

        StateNode next = null;
        
        int score = lastNode.Expand(depth, ref next, ref visitedStates, true);
        if (next.State.CalculateScore() <= 0)
            Completed = true;

        // Remove selected node
        visitedStates.RemoveAll(state => state.Equals(next.State));

        Int2 move = next.Move;
        evolutionPath.AddPoint(move);
        lastNode = next;


        resultPath = evolutionPath;
        return true;
    }

    private Path CalculatePath(StateNode startNode, StateNode endNode)
    {
        Path path = new Path();

        for(StateNode node = null; node != null; node = node.Parent)
        {
            path.AddPoint(node.Move);
        }
        path.Reverse();
        return path;
    }
}
