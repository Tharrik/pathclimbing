﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class StateNode
{
    public Grid State { get; }

    public StateNode Parent { get; private set; }
    public Int2 Move { get { return State.WhitePosition; } }


    public StateNode(Grid grid, StateNode parent = null)
    {
        State = grid;
        Parent = parent;
    }

    /**
     * @param depth current depth of the evaluation. 0 is deeper
     * @param selectedNode child node with best score
     * @param winnerNode best node of all branches
     */
    public int Expand(int depth, ref StateNode selectedNode, ref List<Grid> visitedStates, bool firstDepth)
    {
        int bestScore = int.MaxValue;
        StateNode bestNode = new StateNode(State);

        List<Int2> moves = State.GetWhiteSurroundings();

        // Reserve memory
        visitedStates.Capacity = visitedStates.Count + moves.Count;

        foreach(Int2 move in moves)
        {
            // Simple cycle pruning: If this move goes back, do nothing.
            if (Parent != null && move.Equals(Parent.Move))
                continue;

            // Next move state
            Grid childState = State.Clone();
            childState.MoveWhite(move);

            // Simple cycle pruning: Never visit the same state twice
            if (firstDepth)
            {
                if (visitedStates.Any(grid => grid.Equals(childState)))
                {
                    continue;
                }
                visitedStates.Add(childState);
            }


            StateNode child = new StateNode(childState, this);

            // If solved, return current child
            if (child.State.IsSolved())
            {
                selectedNode = child;
                return 0;
            }

            // If maximum depth, pick best node of all moves
            if (depth <= 0)
            {
                int childScore = childState.CalculateScore();
                if (childScore < bestScore)
                {
                    bestScore = childScore;
                    bestNode = child;
                }
            }
            // If not maximum depth, best node of children nodes
            else
            {
                StateNode childBestNode = null;

                int childScore = child.Expand(depth - 1, ref childBestNode, ref visitedStates, false);
                if (childScore < bestScore)
                {
                    bestScore = childScore;
                    bestNode = child;
                }
            }
        }
        selectedNode = bestNode;
        return bestScore;
    }
}
