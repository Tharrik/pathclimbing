﻿using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class Path
{
    #region PROPERTIES

    [SerializeField]
    public List<Int2> Points { get; private set; }

    public int Count { get { return Points.Count; } }
    public Int2 Last { get { return Points[0];    } }

    #endregion


    #region METHODS

    public Path() {
        Points = new List<Int2>();
    }

    public void AddPoint(Int2 point) {
        Points.Add(point);
    }

    public void Append(Path other)
    {
        Points.AddRange(other.Points);
    }

    public void Reverse()
    {
        Points.Reverse();
    }

    #endregion


    public static Path operator +(Path a, Path b)
    {
        Path result = new Path();
        result.Append(a);
        result.Append(b);
        return result;
    }
}
