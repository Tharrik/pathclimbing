﻿using UnityEngine;


public class GridManager : MonoBehaviour
{
    [SerializeField]
    Grid grid;
    public Grid Grid
    {
        get { return grid; }
    }

    [SerializeField]
    int size = 3;
    public int Size {
        get { return size; }
        set { size = value >= 0 ? value : size; }
    }
    
    [SerializeField]
    int depth = 1;


    private Simulator simulation;
    public int Generation
    {
        get {
            if (simulation == null) return -1;
            return simulation.GenerationCount; }
    }

    public string SizeFromString
    {
        set
        {
            int result;
            if (!int.TryParse(value, out result)) return;
            Size = result;
        }
    }

    #region Singleton
    public static GridManager Instance { get; private set; }

    private void Awake()
    {
        if (!Instance) { Instance = this; }
        else { Destroy(gameObject); }
    }
    #endregion

    #region Events
    public delegate void GridEvent();
    public static event GridEvent OnNewGrid;
    public static event GridEvent OnGridUpdate;

    #endregion

    public void GenerateGrid()
    {
        grid = new Grid(size);
        grid.Randomize();

        if (OnNewGrid != null)
        {
            OnNewGrid();
        }

        NewSimulation();
    }

    public void NewSimulation() {
        if (grid != null)
        {
            simulation = new Simulator(grid, depth);
        }
    }

    public bool NextGeneration(ref Path path)
    {
        if (simulation == null)
            return false;
        
        return simulation.NextGeneration(ref path);
    }

    public bool NextGenerations(int num, ref Path lastPath)
    {
        if (simulation == null)
            return false;

        Path path = null;
        
        for(int count = 0; !simulation.Completed && count < num; ++count)
        {
            simulation.NextGeneration(ref path);
        }

        lastPath = path;
        return true;
    }
}
