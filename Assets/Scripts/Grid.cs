﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

[System.Serializable]
public struct Int2
{
    public static readonly Int2 Zero  = new Int2( 0,  0);
    public static readonly Int2 Up    = new Int2( 1,  0);
    public static readonly Int2 Down  = new Int2(-1,  0);
    public static readonly Int2 Right = new Int2( 0,  1);
    public static readonly Int2 Left  = new Int2( 0, -1);

    public int x;
    public int y;

    public Int2(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public new string ToString()
    {
        return string.Concat("{ ", x, ", ", y, " }" );
    }

    public static Int2 operator+ (Int2 a, Int2 b)
    {
        return new Int2(a.x + b.x, a.y + b.y);
    }

    public static int GetManhattanDistance(Int2 a, Int2 b) {
        return Math.Abs(a.x - b.x) + Math.Abs(a.y - b.y);
    }

}

[System.Serializable]
public class Grid
{
    #region PROPERTIES

    // Cached possible moves
    public static readonly int whiteValue = 0;

    private static Int2[] moves = {
        Int2.Up,
        Int2.Down,
        Int2.Right,
        Int2.Left
    };
    private int[,] grid;

    public Int2 Size { get; private set;}
    public Int2 WhitePosition { get; private set; }

    #endregion


    #region METHODS

    public Grid() {
        Size = new Int2(3, 3);
        SetupPieces();
    }

    public Grid(int inSize) {
        Size = new Int2(inSize, inSize);
        SetupPieces();
    }

    public Grid(Int2 inSize) {
        Size = inSize;
        SetupPieces();
    }

    // From 1 to N, being the last one 0 (white)
    public void SetupPieces()
    {
        grid = new int[Size.x, Size.y];

        int value = 1;
        for (int x = 0; x < Size.x; ++x)
        {
            for (int y = 0; y < Size.y; ++y)
            {
                grid[x, y] = value;
                ++value;
            }
        }
        // Set last to white
        WhitePosition = new Int2(Size.x - 1, Size.y - 1);
        grid[WhitePosition.x, WhitePosition.y] = whiteValue;
    }

    // Make random moves to "randomize" the grid
    public void Randomize()
    {
        SetupPieces();

        Int2 lastPosition = new Int2(-1, -1);

        int randomMoves = Size.x * Size.y * 5;
        for (int i = 0; i < randomMoves; ++i)
        {
            List<Int2> moves = GetWhiteSurroundings();

            moves.Remove(lastPosition);

            lastPosition = WhitePosition;
            MoveWhite(moves[Random.Range(0, moves.Count)]);
        }
    }

    public bool IsSolved()
    {
        int lastValue = 0;
        for (int x = 0; x < Size.x; ++x)
        {
            for (int y = 0; y < Size.y; ++y)
            {
                int currentValue = grid[x, y];

                if (currentValue != 0 && currentValue != lastValue + 1)
                    return false;

                lastValue = currentValue;
            }
        }
        return true;
    }

    public int GetValue(Int2 position) {
        if (IsValid(position))
        {
            return grid[position.x, position.y];
        }
        return -1;
    }

    public bool MoveWhite(Int2 position)
    {
        if (IsValid(position))
        {
            // Move piece to whitePosition
            grid[WhitePosition.x, WhitePosition.y] = grid[position.x, position.y];

            // Set target to white
            WhitePosition = position;
            grid[position.x, position.y] = whiteValue;
            return true;
        }
        return false;
    }

    public bool IsValid(Int2 position) {
        return position.x >= 0 && position.y >= 0 && position.x < Size.x && position.y < Size.y;
    }

    // Consider using GetSurroundings() instead to allow optimizations
    public bool CanMove() {
        return GetWhiteSurroundings().Count > 0;
    }
    
    // @return possible move positions around the white position
    public List<Int2> GetWhiteSurroundings()
    {
        return GetSurroundings(WhitePosition);
    }

    // @return possible move positions around a position
    public List<Int2> GetSurroundings(Int2 position)
    {
        List<Int2> results = new List<Int2>();

        if (!IsValid(position))
            return results;

        foreach (Int2 move in moves)
        {
            Int2 surrounding = position + move;
            if (IsValid(surrounding))
            {
                results.Add(surrounding);
            }
        }
        return results;
    }

    public Int2 GetValueValidPosition(int value) {
        if (value == whiteValue)
            return new Int2(Size.x - 1, Size.y - 1);

        value -= 1;
        return new Int2(value / Size.x, value % Size.x);
    }

    public int CalculateScore() {
        int score = 0;
        for (int x = 0; x < Size.x; ++x)
        {
            for (int y = 0; y < Size.y; ++y)
            {
                int value = grid[x, y];
                
                // Manhattan distance to result position
                score += Int2.GetManhattanDistance(
                    new Int2(x, y),
                    GetValueValidPosition(value)
                );
            }
        }
        return score;
    }
    
    public bool Equals(Grid other)
    {
        if (ReferenceEquals(this, other))
            return true;

        if (!Size.Equals(other.Size))
            return false;

        for (int x = 0; x < Size.x; ++x)
        {
            for (int y = 0; y < Size.y; ++y)
            {
                if (!grid[x, y].Equals(other.grid[x, y]))
                {
                    return false;
                }
            }
        }
        return true;
    }

    public override bool Equals(object obj)
    {
        if (ReferenceEquals(null, obj))
            return false;

        return obj.GetType() == GetType() && Equals((Grid)obj);
    }

    #endregion


    #region CLONE INTERFACE

    public Grid Clone() { return new Grid(this); }

    // Copy constructor
    Grid(Grid other)
    {
        Size = other.Size;
        grid = (int[,])other.grid.Clone();

        WhitePosition = other.WhitePosition;
    }
    #endregion
}
