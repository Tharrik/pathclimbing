﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIPuzzleTile : MonoBehaviour
{
    Text text;
    public int Number
    {
        set { text.text = value == 0 ? "" : value.ToString(); }
    }

    // Start is called before the first frame update
    void Awake()
    {
        text = GetComponentInChildren<Text>();
    }

    

}
