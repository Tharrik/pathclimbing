﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIPuzzleVisualizator : MonoBehaviour
{
    [SerializeField]
    GameObject tilePrefab;

    [SerializeField]
    Text pathText;
    [SerializeField]
    InputField sizeInput;

    GridLayoutGroup layout;

    Grid currentState;
    Path currentPath;
    int currentMoveIndex;

    private float updateTime = 1;
    public int SpeedFactor { get; set; }

    List<UIPuzzleTile> tiles = new List<UIPuzzleTile>();

    // Start is called before the first frame update
    void Start()
    {
        // Catch components
        layout = GetComponent<GridLayoutGroup>();

        // Initialize
        SpeedFactor = 2;
        sizeInput.text = 3.ToString();

        // Subscribe to data event
        GridManager.OnNewGrid += GenerateTiles;
    }

    void ClearTiles()
    {
        for(int i = 0; i < tiles.Count; ++i)
        {
            Destroy(tiles[i].gameObject);
        }

        tiles.Clear();
    }

    void GenerateTiles()
    {
        ClearTiles();

        // Get puzzle
        Grid puzzleState = GridManager.Instance.Grid;
        Int2 puzzleSize = puzzleState.Size;

        layout.cellSize = layout.GetComponent<RectTransform>().sizeDelta / puzzleSize.x;

        for (int x = 0; x < puzzleState.Size.x; ++x)
        {
            for (int y = 0; y < puzzleState.Size.y; y++)
            {
                UIPuzzleTile tile = Instantiate(tilePrefab).GetComponent<UIPuzzleTile>();
                tile.Number = puzzleState.GetValue(new Int2(x, y));
                tile.transform.SetParent(layout.transform);
                tile.transform.localScale = Vector3.one;
                tiles.Add(tile);
            }
        }
    }

    public void NextGeneration()
    {
        StopAllCoroutines();
        currentState = GridManager.Instance.Grid.Clone();
        currentMoveIndex = 0;
        currentPath = new Path();

        if(GridManager.Instance.NextGeneration(ref currentPath))
        {
            ShowPathText();
            currentState.MoveWhite(currentPath.Points[currentMoveIndex++]);
            StartCoroutine(VisualizePath());
        }
        else
        {
            Debug.LogWarning("NEXT GENERATION: NO MORE");
        }
    }

    public void Next100Generations()
    {
        StopAllCoroutines();
        currentState = GridManager.Instance.Grid.Clone();
        currentMoveIndex = 0;
        currentPath = new Path();

        if(GridManager.Instance.NextGenerations(100, ref currentPath))
        {
            ShowPathText();
            currentState.MoveWhite(currentPath.Points[currentMoveIndex++]);
            StartCoroutine(VisualizePath());
        }
        else
        {
            Debug.LogError("ALL GENERATIONS: PATH ERROR");
        }

    }

    public IEnumerator VisualizePath()
    {
        UpdateNumbers(currentState);

        if(currentMoveIndex < currentPath.Count)
        {
            currentState.MoveWhite(currentPath.Points[currentMoveIndex++]);
            yield return new WaitForSeconds(updateTime / SpeedFactor);
            StartCoroutine(VisualizePath());
        }
    }

    public void ShowPathText()
    {
        string text = "GENERATION " + GridManager.Instance.Generation + ":\n";

        if (currentPath != null)
        {
            for (int i = 0; i < currentPath.Count; ++i)
            {
                text = string.Concat(text, "M", i + 1, ": ", currentPath.Points[i].ToString(), ", ");
            }
        }

        text.TrimEnd(',');
        pathText.text = text;
    }

    void UpdateNumbers(Grid state)
    {
        for (int x = 0; x < state.Size.x; ++x)
        {
            for (int y = 0; y < state.Size.y; y++)
            {
                tiles[x * state.Size.y + y].Number = state.GetValue(new Int2(x,y));
            }
        }
    }

}
